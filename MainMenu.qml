import QtQuick 2.7
import QtQuick.XmlListModel 2.0

Item {
    property var parentWith: null
    property var parentHeight: null
    property var systemsCategoryCount: null
    property var systemsCurrentCategoryID: null

    Rectangle{
        id: topBar
        width:parentWith
        height: vpx(50)
        anchors.top: parent.top
        anchors.left: parent.left
        // color palette in Theme.qml
        color: themeColor.main

        Row{
            height: topBar.height
            spacing: vpx(30)

            Repeater { model: api.collections.count
                Text {
                    text: api.collections.get(index).name
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.leftMargin: vpx(200)
                    anchors.rightMargin: vpx(10)
                    // choose font style in theme.qml
                    font.family: fontBoldP8.name
                    // color palette in theme.qml
                    color: themeColor.text
                    font.pixelSize: topBar.height / 2
                    font.capitalization: Font.AllUppercase
                }
            }
        }
    }
}
