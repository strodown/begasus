import QtQuick 2.0
import QtMultimedia 5.0
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2

FocusScope {

    MainMenu{
        parentWith: parent.width
        parentHeight: parent.height
    }
    // Color palette set with 'themeColor.main' or else
    property var themeColor: {
        return {
            main:           "#395451",
            secondary:      "#202a44",
            highlight:      "#f00980",
            text:           "#062f51",
        }
    }
    // choose font with : font.family: fontBoldP.name
    // Bold font
    FontLoader { id: fontBoldP; source: "assets/fonts/8BITOPERATORPLUS-BOLD.TTF" }
    FontLoader { id: fontBoldP8; source: "assets/fonts/8BITOPERATORPLUS8-BOLD.TTF" }
    FontLoader { id: fontBoldPssc; source: "assets/fonts/8BITOPERATORPLUSSC-BOLD.TTF" }
    // regular font
    FontLoader { id: fontRegularP; source: "assets/fonts/8BITOPERATORPLUS-REGULAR.TTF" }
    FontLoader { id: fontRegularP8; source: "assets/fonts/8BITOPERATORPLUS8-REGULAR.TTF" }
    FontLoader { id: fontRegularPssc; source: "assets/fonts/8BITOPERATORPLUSSC-REGULAR.TTF" }
}
